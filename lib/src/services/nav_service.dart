import 'package:flutter/cupertino.dart';

class Nav {
  static void obj(BuildContext context, Widget route) {
    Navigator.of(context).push(CupertinoPageRoute(builder: (_) => route));
  }

  static void route(BuildContext context, String route) {
    Navigator.of(context).pushNamed(route);
  }
}
