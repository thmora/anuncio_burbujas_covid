import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static final SharedPrefs _instance = SharedPrefs._internal();

  factory SharedPrefs() => _instance;

  SharedPrefs._internal();

  late SharedPreferences _prefs;

  initPrefs() async => _prefs = await SharedPreferences.getInstance();

  bool get loggedIn => this.id != "" ? true : false;

  String get id => _prefs.getString('id') ?? '';

  set id(String value) => _prefs.setString('id', value);

  String get lastEmail => _prefs.getString('lastEmail') ?? '';

  set lastEmail(String value) => _prefs.setString('lastEmail', value);
}
