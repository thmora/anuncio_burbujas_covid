import 'package:anuncio_burbujas_covid/src/shared/custom_snackbar.dart';
import 'package:anuncio_burbujas_covid/src/shared/perfiles.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/models/user.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FirebaseController {
  // USER FUNCTIONS

  static Future<Bubble> get myBubble async {
    final qs = await FirebaseFirestore.instance
        .collection("bubbles")
        .doc(Auth.user!.bubbleID)
        .withConverter(
          fromFirestore: (snapshot, _) =>
              Bubble.fromDocument(snapshot.data()!, snapshot.id),
          toFirestore: (Bubble bubble, _) => bubble.toJson,
        )
        .get();
    return qs.data()!;
  }

  // ADMIN FUNCTIONS

  static Future<List<User>> usersByBubbleId(String bubbleID) async {
    final qs = await FirebaseFirestore.instance
        .collection("users")
        .where("bubbleID", isEqualTo: bubbleID)
        .withConverter(
          fromFirestore: (snapshot, _) =>
              User.fromDocument(snapshot.data()!, snapshot.id),
          toFirestore: (User user, _) => user.toJson,
        )
        .get();
    List<User> users = [];
    print(qs.docs.length);
    for (QueryDocumentSnapshot<User> u in qs.docs) {
      users.add(u.data());
    }
    print(users);
    return users;
  }

  static Future<List<Bubble>> get bubbles async {
    final qs = await FirebaseFirestore.instance
        .collection("bubbles")
        .where("adminID", isEqualTo: Auth.user!.docID)
        .withConverter(
          fromFirestore: (snapshot, _) =>
              Bubble.fromDocument(snapshot.data()!, snapshot.id),
          toFirestore: (Bubble b, _) => b.toJson,
        )
        .get();

    List<Bubble> bs = [];
    qs.docs.map((QueryDocumentSnapshot<Bubble> b) => bs.add(b.data())).toList();
    return bs;
  }

  static Future<void> createUser(
    String email,
    String identificador,
    String bubbleID,
    BuildContext context,
  ) async {
    if (_validate(Auth.user!.isAdmin, context)) {
      final bool usuarioOcupado = await Auth.usuarioExiste(email);
      if (!usuarioOcupado) {
        final User newUser = User(
          bubbleID: bubbleID,
          email: email,
          identificador: identificador,
          perfil: Perfiles.user.str,
          password: Auth.encriptarContra("12345678"),
          entityName: Auth.user!.entityName,
          entityType: Auth.user!.entityType,
        );
        FirebaseFirestore.instance
            .collection("users")
            .doc()
            .set(newUser.toJson);
        showCustomSnackbar(context, "Usuario creado.", Colors.blue);
      } else {
        showCustomSnackbar(context, "Email ocupado", Colors.red);
      }
    }
  }

  static void createBubble(Bubble bubble, BuildContext context) {
    if (_validate(Auth.user!.isAdmin, context)) {
      FirebaseFirestore.instance.collection("bubbles").doc().set(bubble.toJson);
      showCustomSnackbar(context, "Burbuja creada.", Colors.blue);
    }
  }

  static void updateBubble(Bubble bubble, BuildContext context) {
    if (_validate(Auth.user!.isAdmin, context)) {
      FirebaseFirestore.instance
          .collection("bubbles")
          .doc(bubble.id)
          .update(bubble.toJson);
      showCustomSnackbar(context, "Burbuja creada.", Colors.blue);
    }
  }

  static void deleteBubble(String id, BuildContext context) {
    if (_validate(Auth.user!.isAdmin, context)) {
      FirebaseFirestore.instance.collection("bubbles").doc(id).delete();
      showCustomSnackbar(context, "Burbuja eliminada.", Colors.blue);
    }
  }

  // SUPER ADMIN FUNCTIONS

  static Future<QuerySnapshot<User>> get admins async {
    final qs = await FirebaseFirestore.instance
        .collection("users")
        .where("entityName", isEqualTo: Auth.user!.entityName)
        .where("perfil", isEqualTo: Perfiles.admin.str)
        .withConverter(
          fromFirestore: (snapshot, _) =>
              User.fromDocument(snapshot.data()!, snapshot.id),
          toFirestore: (User user, _) => user.toJson,
        )
        .get();
    return qs;
  }

  static void deleteAdmin(String id, BuildContext context) {
    if (_validate(Auth.user!.isSuperAdmin, context)) {
      FirebaseFirestore.instance.collection("users").doc(id).delete();
      showCustomSnackbar(context, "Administrador eliminado.", Colors.blue);
    }
  }

  static Future<void> createAdmin(String email, BuildContext context) async {
    bool userExists = await Auth.usuarioExiste(email);

    if (!userExists) {
      final newAdmin = User(
        email: email,
        perfil: Perfiles.admin.str,
        entityName: Auth.user!.entityName,
        identificador: "Administrador",
        entityType: Auth.user!.entityType,
        password: Auth.encriptarContra("12345678"),
      );
      if (_validate(Auth.user!.isSuperAdmin, context)) {
        FirebaseFirestore.instance
            .collection("users")
            .doc()
            .set(newAdmin.toJson);
        showCustomSnackbar(context, "Administrador creado.", Colors.blue);
      }
    } else {
      showCustomSnackbar(context, "El Email esta ocupado.", Colors.red);
    }
  }

  // VALIDATOR FOR FUNCTIONS

  static bool _validate(bool validator, BuildContext context) {
    if (validator) {
      showCustomSnackbar(context, "Procesando..", Colors.green);
      return true;
    } else {
      showCustomSnackbar(
        context,
        "Usted no puede realizar esta operacion",
        Colors.red,
      );
      return false;
    }
  }

  static Future<void> createEntity(
    String email,
    String password,
    String entityName,
    String type,
    BuildContext context,
  ) async {
    final qs = await FirebaseFirestore.instance
        .collection("users")
        .where("entityName", isEqualTo: entityName)
        .where("entityType", isEqualTo: type)
        .limit(1)
        .get();

    if (qs.docs.isEmpty) {
      final newSuperAdmin = User(
        email: email,
        password: Auth.encriptarContra(password),
        perfil: Perfiles.superadmin.str,
        entityName: entityName,
        identificador: "SuperAdmin",
        entityType: type,
      );
      FirebaseFirestore.instance
          .collection("users")
          .doc()
          .set(newSuperAdmin.toJson);
      showCustomSnackbar(context, "Entidad creada.", Colors.blue);
      Navigator.pop(context);
      Navigator.pop(context);
      await Auth.loginWithEmailAndPassword(email, password, context);
    } else {
      showCustomSnackbar(
        context,
        "Lo sentimos, ya existe una entidad con ese nombre.",
        Colors.red,
      );
    }
  }
}
