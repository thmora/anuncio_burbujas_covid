import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/shared/confirm_alert_dialog.dart';
import 'package:flutter/material.dart';

class LogOutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text("Cerrar Sesión", style: TextStyle(color: Colors.white)),
      trailing: Icon(Icons.exit_to_app, color: Colors.white),
      onTap: () => confirmAlertDialog(
        context,
        "Desea cerrar sesión?",
        () async {
          await Auth.logOut();
          Navigator.pop(context);
        } ,
      ),
    );
  }
}
