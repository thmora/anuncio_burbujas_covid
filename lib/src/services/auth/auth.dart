import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:anuncio_burbujas_covid/src/models/user.dart';
import 'package:anuncio_burbujas_covid/src/shared/custom_snackbar.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth_state.dart';
import 'package:anuncio_burbujas_covid/src/services/shared_preferences/shared_prefs.dart';

class Auth {
  static final _prefs = SharedPrefs();
  static late User? _user;
  static User? get user => _user;

  static Future<void> loginWithEmailAndPassword(
      String email, String password, BuildContext context) async {
    showCustomSnackbar(context, "Iniciando sesion", Colors.green);
    final QuerySnapshot<User> res = await FirebaseFirestore.instance
        .collection("users")
        .limit(1)
        .where("email", isEqualTo: email)
        .withConverter(
          fromFirestore: (snapshot, _) =>
              User.fromDocument(snapshot.data()!, snapshot.id),
          toFirestore: (User user, _) => user.toJson,
        )
        .get();

    if (res.docs.isNotEmpty) {
      final String contraEncriptada = encriptarContra(password);
      if (res.docs.first.data().password == contraEncriptada) {
        _user = res.docs.first.data();
        _prefs.id = res.docs.first.data().docID;
        AuthStatePage.instance.state = AuthState.authorized;
      } else {
        showCustomSnackbar(
            context, "Usuario o contraseña incorrectas.", Colors.red);
        AuthStatePage.instance.state = AuthState.unauthorized;
      }
    } else {
      showCustomSnackbar(context, "El email no esta registrado", Colors.red);
    }
  }

  static Future<void> recoverUser() async {
    final DocumentSnapshot<Map<String, dynamic>> res = await FirebaseFirestore
        .instance
        .collection("users")
        .doc(_prefs.id)
        .get();

    _user = User.fromDocument(res.data()!, res.id);
    _prefs.id = res.id;
    AuthStatePage.instance.state = AuthState.authorized;
  }

  static Future<void> logOut() async {
    _prefs.id = "";
    _user = null;
    AuthStatePage.instance.state = AuthState.unauthorized;
  }

  static String encriptarContra(String contra) {
    final List<int> cBytes = utf8.encode(contra);
    final Digest digest = sha1.convert(cBytes);
    final String contraEncriptada = digest.toString();
    return contraEncriptada;
  }

  static Future<void> cambiarContra(String newPassword) async {
    final String contraEncriptada = encriptarContra(newPassword);

    await FirebaseFirestore.instance
        .collection("usuarios")
        .doc(_user!.docID)
        .update({"password": contraEncriptada});
  }

  static Future<bool> usuarioExiste(String email) async {
    final QuerySnapshot res = await FirebaseFirestore.instance
        .collection("users")
        .where("email", isEqualTo: email)
        .limit(1)
        .get();

    final bool existe = res.docs.isNotEmpty && res.docs.first.exists;
    print(existe);
    return existe;
  }
}
