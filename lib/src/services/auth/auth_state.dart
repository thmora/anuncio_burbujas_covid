import 'package:flutter/material.dart';

enum AuthState { authorized, unauthorized }

class AuthStatePage with ChangeNotifier {
  AuthStatePage._();
  static final instance = AuthStatePage._();
  AuthState _showingState = AuthState.unauthorized;
  AuthState get state => this._showingState;
  set state(AuthState newState) {
    this._showingState = newState;
    notifyListeners();
  }
}
