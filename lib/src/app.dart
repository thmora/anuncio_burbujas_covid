import 'package:anuncio_burbujas_covid/src/routes/routes.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Anuncio Burbujas Covid',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/Landing',
      routes: Routes.routes,
    );
  }
}
