import 'package:flutter/material.dart';

void mbs(BuildContext context, String title, Widget child) {
  showModalBottomSheet(
    backgroundColor: Colors.transparent,
    context: context,
    builder: (_) {
      return Container(
        height: MediaQuery.of(context).size.height * .5,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
          color: Colors.white,
        ),
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  title,
                  style: TextStyle(fontSize: 20),
                ),
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: child,
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
