import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/bubble_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/edit/edit_bubble_page.dart';
import 'package:anuncio_burbujas_covid/src/services/nav_service.dart';
import 'package:anuncio_burbujas_covid/src/shared/confirm_alert_dialog.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

extension BubbleExtension on Bubble {
  Widget get show {
    if (Auth.user!.isAdmin) return _BubbleListTile(this);
    if (Auth.user!.isUser) return _bubbleBody();
    return SizedBox.shrink();
  }

  Widget _bubbleBody() {
    Color _bgColor() {
      Color color = Colors.white;
      if (status == "Verde") color = Colors.green;
      if (status == "Amarillo") color = Colors.amberAccent[700]!;
      if (status == "Rojo") color = Colors.red;
      return color;
    }

    TextStyle _style() => TextStyle(color: Colors.white, fontSize: 22);
    Widget _text(String t) => Text(t, style: _style());

    return Container(
      color: _bgColor(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _text("Burbuja N$numero $firstDivider $secondDivider"),
              _text(statusMsg),
              _text("Ultima actualización: ${_getLastUpdate()}"),
            ],
          ),
        ),
      ),
    );
  }

  String _getLastUpdate() {
    DateFormat dateFormat = DateFormat("dd/MM HH:mm");
    DateTime fromMillSinceEpoch = DateTime.fromMillisecondsSinceEpoch(
      int.parse(lastUpdate),
    );
    final String timestamp = dateFormat.format(fromMillSinceEpoch);
    return timestamp;
  }
}

class _BubbleListTile extends StatelessWidget {
  _BubbleListTile(this.bubble);
  final Bubble bubble;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
          "Burbuja N${bubble.numero} - ${bubble.firstDivider} ${bubble.secondDivider}"),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            icon: Icon(
              Icons.person_outline,
              color: Colors.green,
            ),
            onPressed: () => Nav.obj(context, BubblePage(bubble)),
          ),
          IconButton(
            icon: Icon(
              Icons.edit_outlined,
              color: Colors.amber,
            ),
            onPressed: () => Nav.obj(context, EditBubblePage(bubble)),
          ),
          IconButton(
            icon: Icon(
              Icons.delete_forever_outlined,
              color: Colors.red,
            ),
            onPressed: () {
              confirmAlertDialog(
                context,
                "¿Desea eliminar a la burbuja?",
                () => FirebaseController.deleteBubble(bubble.id, context),
              );
            },
          ),
        ],
      ),
    );
  }
}
