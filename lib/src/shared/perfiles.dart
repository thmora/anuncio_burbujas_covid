enum Perfiles { superadmin, admin, user }

extension perfiles on Perfiles {
  String get str {
    String perfil = "";
    switch (this.index) {
      case 0:
        return "superadmin";
      case 1:
        return "admin";
      case 2:
        return "user";
    }

    return perfil;
  }
}
