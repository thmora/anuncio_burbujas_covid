import 'package:flutter/material.dart';

class LoginScreenPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = Colors.white;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 25.0;
    final path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height * .35);
    path.quadraticBezierTo(size.width * .25, size.height * 0.45,
        size.width * .5, size.height * 0.35);
    path.quadraticBezierTo(
        size.width * .75, size.height * 0.25, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
class CurvePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = Colors.white;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 25.0;

    final path = Path();
    /* CURVADO */
    path.moveTo(0, 0);
    path.lineTo(0, size.height * .6);
    path.quadraticBezierTo(
        size.width * .2, size.height * 0.7, size.width, size.height * 0.45);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}



/* path.moveTo(0, 0);
    path.lineTo(0, size.height * .7);
    path.quadraticBezierTo(
        size.width * .2, size.height * 0.8, size.width, size.height * 0.45);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint); */