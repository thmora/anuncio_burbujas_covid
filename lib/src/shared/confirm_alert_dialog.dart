import 'package:anuncio_burbujas_covid/src/shared/custom_alert_dialog.dart';
import 'package:flutter/material.dart';

void confirmAlertDialog(
  BuildContext context,
  String mensaje,
  void Function() onConfirm,
) {
  customAlertDialog(
    context,
    [
      Text(
        mensaje,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 22),
      ),
      SizedBox(height: 20),
      Row(
        children: <Widget>[
          _btn('Cancelar', () => Navigator.pop(context), Colors.red),
          SizedBox(width: 20),
          _btn("Confirmar", onConfirm, Colors.green)
        ],
      )
    ],
  );
}

Widget _btn(String msg, void Function() action, Color color) {
  return Expanded(
    child: ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith((states) => color),
      ),
      child: Text('$msg'),
      onPressed: action,
    ),
  );
}
