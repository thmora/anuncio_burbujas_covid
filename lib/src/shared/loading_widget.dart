import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  LoadingWidget([this.msg = ""]);
  final String msg;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: msg.isEmpty
          ? CircularProgressIndicator()
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(msg),
                ),
              ],
            ),
    );
  }
}
