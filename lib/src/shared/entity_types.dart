enum EntityTypes { Primaria, Secundaria, Empresa }

extension entityTypes on EntityTypes {
  String get str {
    String entity = "";
    switch (this.index) {
      case 0:
        return "Primaria";
      case 1:
        return "Secundaria";
      case 2:
        return "Empresa";
    }
    return entity;
  }
}

extension listEntityTypes on List<EntityTypes> {
  List<String> get toStr => ["Primaria", "Secundaria", "Empresa"];
}
