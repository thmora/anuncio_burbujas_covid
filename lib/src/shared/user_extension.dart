import 'package:anuncio_burbujas_covid/src/shared/confirm_alert_dialog.dart';
import 'package:anuncio_burbujas_covid/src/shared/perfiles.dart';
import 'package:anuncio_burbujas_covid/src/models/user.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:flutter/material.dart';

extension UserExtension on User {
  Widget get show {
    if (this.perfil == Perfiles.admin.str) {
      return _UserListTile(
        email: this.email,
        id: this.docID,
        deleteMsg: "¿Desea eliminar al administrador?",
      );
    }
    if (this.perfil == Perfiles.user.str) {
      return _UserListTile(
        email: this.email,
        id: this.docID,
        deleteMsg: "¿Desea eliminar al usuario?",
      );
    }
    return SizedBox.shrink();
  }
}

class _UserListTile extends StatelessWidget {
  _UserListTile({this.email = "", this.id = "", this.deleteMsg = ""});
  final String email;
  final String id;
  final String deleteMsg;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(email),
      trailing: IconButton(
        icon: Icon(
          Icons.delete_forever_rounded,
          color: Colors.red,
        ),
        onPressed: () {
          confirmAlertDialog(
            context,
            deleteMsg,
            () => FirebaseController.deleteAdmin(id, context),
          );
        },
      ),
    );
  }
}
