import 'package:flutter/material.dart';

void showCustomSnackbar(BuildContext context, String msg,
    [MaterialColor color = Colors.green, IconData iconData = Icons.face]) {
  ScaffoldMessenger.of(context)
    ..hideCurrentSnackBar()
    ..showSnackBar(
      SnackBar(
        duration: Duration(seconds: 5),
        content: Row(
          children: [
            Expanded(
              child: Text(msg, style: TextStyle(color: Colors.white)),
            ),
            iconData == Icons.face
                ? _icon(color)
                : Icon(iconData, color: Colors.white),
          ],
        ),
        backgroundColor: color,
      ),
    );
}

Widget _icon(MaterialColor color) {
  Widget icon = SizedBox();
  if (color == Colors.red)
    icon = Icon(
      Icons.report_problem_rounded,
      color: Colors.white,
    );
  if (color == Colors.green) icon = CircularProgressIndicator();
  if (color == Colors.blue)
    icon = Icon(
      Icons.check_circle_outline_outlined,
      color: Colors.white,
    );
  return icon;
}
