import 'package:anuncio_burbujas_covid/src/pages/cambiar_contra/cambiar_contra.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/admin_home_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/create/new_bubble_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/super_admin/new_admin_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/super_admin/superadmin_home.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/user/user_home_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/landing/landing_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/login/login_page.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Routes {
  static final Map<String, StatelessWidget Function(BuildContext)> routes = {
    '/Landing': (_) => ChangeNotifierProvider(
          create: (_) => AuthStatePage.instance,
          child: LandingPage(),
        ),
    '/SuperAdminHome': (_) => SuperAdminHomePage(),
    '/AdminHome': (_) => AdminHomePage(),
    '/UserHome': (_) => UserHomePage(),
    '/Login': (_) => LoginPage(),
    "/newAdminPage": (_) => NewAdminPage(),
    "/newBubblePage": (_) => NewBubblePage(),
    "/changePasswordPage": (_) => ChangePasswordPage(),
  };
}
