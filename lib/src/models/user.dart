import 'package:anuncio_burbujas_covid/src/shared/perfiles.dart';

class User {
  final String docID;
  final String email;
  final String perfil;
  final String identificador;
  final String password;
  final String bubbleID;
  final String entityName;
  final String entityType;

  User({
    this.docID = "",
    this.email = "",
    this.perfil = "",
    this.password = "",
    this.identificador = "",
    this.bubbleID = "",
    this.entityName = "",
    this.entityType = "",
  });

  bool get isSuperAdmin => perfil == Perfiles.superadmin.str;
  bool get isAdmin => perfil == Perfiles.admin.str;
  bool get isUser => perfil == Perfiles.user.str;

  User.fromDocument(Map<String, Object?> data, String docid)
      : this(
          docID: docid,
          email: data["email"] as String,
          perfil: data["perfil"] as String,
          password: data["password"] as String,
          identificador: data["identificador"] as String,
          bubbleID: data["bubbleID"] as String,
          entityName: data["entityName"] as String,
          entityType: data["entityType"] as String,
        );

  Map<String, Object?> get toJson => {
        "email": email,
        "perfil": perfil,
        "password": password,
        "identificador": identificador,
        "bubbleID": bubbleID,
        "entityName": entityName,
        "entityType": entityType,
      };
}
