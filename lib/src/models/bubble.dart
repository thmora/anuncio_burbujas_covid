class Bubble {
  final String id;
  final String adminID;
  final String numero;
  final String secondDivider;
  final String firstDivider;
  final String status;
  final String statusMsg;
  final String lastUpdate;

  Bubble({
    this.id = "",
    this.adminID = "",
    this.numero = "1",
    this.firstDivider = "",
    this.secondDivider = "",
    this.status = "ok",
    this.statusMsg = "ok",
    this.lastUpdate = "",
  });

  Bubble.fromDocument(Map<String, Object?> data, String docid)
      : this(
          id: docid,
          adminID: data["adminID"] as String,
          numero: data["numero"] as String,
          firstDivider: data["firstDivider"] as String,
          secondDivider: data["secondDivider"] as String,
          status: data["status"] as String,
          statusMsg: data["statusMsg"] as String,
          lastUpdate: data["lastUpdate"] as String,
        );

  Map<String, Object?> get toJson => {
        "adminID": adminID,
        "numero": numero,
        "firstDivider": firstDivider,
        "secondDivider": secondDivider,
        "status": status,
        "statusMsg": statusMsg,
        "lastUpdate": lastUpdate,
      };

  Bubble copyWith({
    String? numero,
    String? firstDivider,
    String? secondDivider,
    String? status,
    String? statusMsg,
    String? lastUpdate,
  }) {
    return Bubble(
      numero: numero ?? this.numero,
      firstDivider: firstDivider ?? this.firstDivider,
      secondDivider: secondDivider ?? this.secondDivider,
      status: status ?? this.status,
      statusMsg: statusMsg ?? this.statusMsg,
      lastUpdate: lastUpdate ?? this.lastUpdate,
      adminID: adminID,
      id: id,
    );
  }
}
