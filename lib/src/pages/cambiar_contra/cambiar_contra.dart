import 'dart:convert';

import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/shared/custom_snackbar.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';

class ChangePasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cambiar Contraseña'),
      ),
      body: ChangePasswordBody(),
    );
  }
}

class ChangePasswordBody extends StatefulWidget {
  @override
  _ChangePasswordBodyState createState() => _ChangePasswordBodyState();
}

class _ChangePasswordBodyState extends State<ChangePasswordBody> {
  bool cambiarContra = false;

  final _key = GlobalKey<FormState>();
  final _contraActual = TextEditingController();
  final _contraNueva = TextEditingController();
  final _confirmarContraNueva = TextEditingController();

  Widget _campo(TextEditingController controller, String label, IconData icon,
      [bool obscureText = false]) {
    return Container(
      child: TextFormField(
        obscureText: obscureText,
        controller: controller,
        validator: (value) {
          return value!.isEmpty ? 'El campo no puede estar vacio!' : null;
        },
        decoration: InputDecoration(
          icon: Icon(icon, color: Colors.black),
          labelText: label,
        ),
      ),
      margin: EdgeInsets.fromLTRB(30, 0, 30, 10),
      padding: EdgeInsets.only(bottom: 10),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: [
        Form(
          key: _key,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _campo(_contraActual, 'Contraseña Actual', Icons.lock, true),
              _campo(_contraNueva, 'Nueva Contraseña', Icons.lock, true),
              _campo(_confirmarContraNueva, 'Confirmar Nueva Contraseña',
                  Icons.lock, true),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  child: Text('Guardar Cambios'),
                  onPressed: () async {
                    final List<int> cBytes = utf8.encode(_contraActual.text);
                    final Digest digest = sha1.convert(cBytes);
                    final String contraActualEncriptada = digest.toString();

                    if (_key.currentState!.validate()) {
                      if (contraActualEncriptada == Auth.user!.password &&
                          _contraNueva.text == _confirmarContraNueva.text) {
                        await Auth.cambiarContra(_contraNueva.text)
                            .whenComplete(
                          () => showCustomSnackbar(
                            context,
                            "Contraseña Cambiada",
                            Colors.green,
                          ),
                        );
                      } else {
                        showCustomSnackbar(
                          context,
                          "Revise las contraseñas",
                        );
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
