import 'package:anuncio_burbujas_covid/src/shared/loading_widget.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/home_page_delegator.dart';
import 'package:anuncio_burbujas_covid/src/pages/login/login_page.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth_state.dart';
import 'package:anuncio_burbujas_covid/src/services/shared_preferences/shared_prefs.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageAuthState createState() => _LandingPageAuthState();
}

class _LandingPageAuthState extends State<LandingPage> {
  Widget showByState(AuthState state) {
    final _prefs = SharedPrefs();
    print("");
    print("Logged In: ${_prefs.loggedIn}");
    print("AuthState: $state");
    print("");
    if (_prefs.loggedIn == false) {
      return LoginPage();
    } else {
      // loggedIn true
      if (AuthState.unauthorized == state) {
        return FutureBuilder(
          future: Auth.recoverUser(),
          builder: (_, s) {
            return Scaffold(
              body: LoadingWidget("Recuperando datos del usuario"),
            );
          },
        );
      } else {
        return HomePageDelegator();
      }
    }
  }

  @override
  Widget build(BuildContext context) =>
      showByState(Provider.of<AuthStatePage>(context).state);
}
