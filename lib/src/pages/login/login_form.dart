import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:anuncio_burbujas_covid/src/services/shared_preferences/shared_prefs.dart';
import 'package:anuncio_burbujas_covid/src/shared/confirm_alert_dialog.dart';
import 'package:anuncio_burbujas_covid/src/shared/custom_snackbar.dart';
import 'package:anuncio_burbujas_covid/src/shared/entity_types.dart';
import 'package:anuncio_burbujas_covid/src/shared/mbs.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _prefs = SharedPrefs();

  @override
  void initState() {
    super.initState();
    _email.text = _prefs.lastEmail;
    recordarUsuario = _prefs.lastEmail.isNotEmpty;
  }

  final _key = GlobalKey<FormState>();
  final _email = TextEditingController();
  final _entityName = TextEditingController();
  final _password = TextEditingController();
  bool recordarUsuario = false;
  String type = EntityTypes.Primaria.str;
  List<String> types = EntityTypes.values.toStr;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _key,
      child: ListView(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            padding: const EdgeInsets.only(bottom: 10),
            child: TextFormField(
              controller: _email,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                icon: Icon(Icons.email_outlined),
                labelText: "Ingrese su email",
              ),
              validator: (value) {
                if (value!.isNotEmpty) {
                  if ((value.contains("@")) && (value.contains(".com"))) {
                    return null;
                  } else {
                    return "Compruebe el email";
                  }
                } else {
                  return "Necesita ingresar el email";
                }
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            child: TextFormField(
              controller: _password,
              obscureText: true,
              decoration: InputDecoration(
                icon: Icon(Icons.lock_outline),
                labelText: "Ingrese su contraseña",
              ),
              validator: (value) {
                if (value!.isNotEmpty) {
                  if (value.length < 8) {
                    return "El nombre de la entidad es muy corto.";
                  }
                  return null;
                } else {
                  return "Necesita ingresar la contraseña";
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: CheckboxListTile(
              value: recordarUsuario,
              onChanged: (v) {
                setState(() {
                  recordarUsuario = v!;
                });
              },
              title: Text(
                "Recordar Usuario",
                style: TextStyle(fontSize: 18),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: ElevatedButton(
              onPressed: () async => validate(),
              child: Text(
                "Ingresar",
                style: TextStyle(fontSize: 22),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: OutlinedButton(
              onPressed: () async => _createEntityHandler(),
              child: Text(
                "Crear una nueva entidad",
                style: TextStyle(fontSize: 18, color: Colors.grey),
              ),
              style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.resolveWith((states) => Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _createEntityHandler() async {
    final _entityKey = GlobalKey<FormState>();
    if (_key.currentState!.validate() &&
        !(await Auth.usuarioExiste(_email.text))) {
      mbs(
        context,
        "Crear una nueva entidad",
        Form(
          key: _entityKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 30),
                  padding: const EdgeInsets.all(16),
                  child: TextFormField(
                    controller: _entityName,
                    decoration: InputDecoration(
                      icon: Icon(Icons.title),
                      labelText: "Ingrese el nombre de la nueva entidad",
                    ),
                    validator: (value) {
                      if (value!.isNotEmpty) {
                        if (value.length < 2) {
                          return "El nombre de la entidad es muy corto.";
                        }
                        if (value.contains(" ")) {
                          return "No debe contener espacios";
                        } else
                          return null;
                      } else {
                        return "Necesita ingresar un nombre";
                      }
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 32),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.class_),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Text('Tipo de entidad:'),
                          ),
                          DropdownButton<String>(
                            items: types
                                .map(
                                  (String showedOption) =>
                                      DropdownMenuItem<String>(
                                    value: showedOption,
                                    child: Text(showedOption),
                                  ),
                                )
                                .toList(),
                            onChanged: (newSelectedOption) {
                              setState(() {
                                type = newSelectedOption!;
                              });
                            },
                            value: type,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: OutlinedButton(
                    onPressed: () {
                      if (_entityKey.currentState!.validate()) {
                        confirmAlertDialog(
                          context,
                          "Confirma que quiere crear la entidad '${_entityName.text}'",
                          () async => FirebaseController.createEntity(
                            _email.text,
                            _password.text,
                            _entityName.text,
                            type,
                            context,
                          ),
                        );
                      }
                    },
                    child: Text(
                      "Crear una nueva entidad",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      showCustomSnackbar(
          context, "El email ya se encuentra registrado", Colors.red);
    }
  }

  Future<void> validate() async {
    if (_key.currentState!.validate()) {
      await Auth.loginWithEmailAndPassword(
        _email.text,
        _password.text,
        context,
      );
      if (recordarUsuario) {
        setState(() {
          _prefs.lastEmail = _email.text;
        });
      } else {
        setState(() {
          _prefs.lastEmail = "";
        });
      }
    }
  }
}
