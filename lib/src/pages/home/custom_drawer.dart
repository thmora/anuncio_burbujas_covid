import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/log_out_button.dart';
import 'package:anuncio_burbujas_covid/src/services/nav_service.dart';
import 'package:anuncio_burbujas_covid/src/shared/custom_painters.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Drawer(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.1, 0.4, 0.7, 0.9],
            colors: [
              Colors.deepPurpleAccent,
              Colors.deepPurple,
              Colors.blue,
              Colors.blueAccent,
            ],
          ),
        ),
        child: CustomPaint(
          painter: CurvePaint(),
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: size.height * .1),
                SizedBox(height: 10),
                Text(
                  '${Auth.user!.identificador}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  '${Auth.user!.email}',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12),
                ),
                Spacer(),
                ListTile(
                  title: Text(
                    "Cambiar contraseña",
                    style: TextStyle(color: Colors.white),
                  ),
                  trailing: Icon(Icons.lock_outline, color: Colors.white),
                  onTap: () => Nav.route(context, "/changePasswordPage"),
                ),
                LogOutButton(),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
