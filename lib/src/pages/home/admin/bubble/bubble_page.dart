import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/create/new_user_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/edit/edit_bubble_page.dart';
import 'package:anuncio_burbujas_covid/src/services/nav_service.dart';
import 'package:anuncio_burbujas_covid/src/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/models/user.dart';
import 'package:anuncio_burbujas_covid/src/shared/user_extension.dart';

class BubblePage extends StatelessWidget {
  BubblePage(this.bubble);
  final Bubble bubble;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            'Burbuja ${bubble.numero} ${bubble.firstDivider}${bubble.secondDivider}'),
        actions: [
          IconButton(
            icon: Icon(Icons.edit_outlined),
            tooltip: "Editar Burbuja",
            onPressed: () => Nav.obj(context, EditBubblePage(bubble)),
          ),
        ],
      ),
      body: BubblePageBody(bubble),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Crear un usuario y ligarlo a la burbuja",
        onPressed: () => Nav.obj(context, NewUserPage(bubble: bubble)),
      ),
    );
  }
}

class BubblePageBody extends StatefulWidget {
  BubblePageBody(this.bubble);
  final Bubble bubble;

  @override
  _BubblePageBodyState createState() => _BubblePageBodyState();
}

class _BubblePageBodyState extends State<BubblePageBody> {
  List<User> users = [];

  @override
  void initState() {
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    final us = await FirebaseController.usersByBubbleId(widget.bubble.id);
    setState(() {
      users = us;
    });
  }

  Widget _buildUsers() {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(
        parent: const BouncingScrollPhysics(),
      ),
      itemCount: users.length,
      itemBuilder: (_, idx) => users[idx].show,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<User>>(
      future: FirebaseController.usersByBubbleId(widget.bubble.id),
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data!.isEmpty) {
          return Center(child: Text("No hay usuarios en la burbuja"));
        } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
          return RefreshIndicator(
            child: _buildUsers(),
            onRefresh: () => refresh(),
          );
        } else {
          return LoadingWidget("Buscando usuarios de la burbuja");
        }
      },
    );
  }
}
