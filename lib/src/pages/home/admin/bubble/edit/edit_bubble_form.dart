import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';

class EditBubbleForm extends StatefulWidget {
  EditBubbleForm(this.bubble);
  final Bubble bubble;
  @override
  _EditBubbleFormState createState() => _EditBubbleFormState();
}

class _EditBubbleFormState extends State<EditBubbleForm> {
  final _key = GlobalKey<FormState>();
  String _estado = "";
  final _statusMsg = TextEditingController();

  @override
  void initState() {
    super.initState();
    _estado = widget.bubble.status;
    _statusMsg.text = widget.bubble.statusMsg;
  }

  Widget _estadoRadio(String value) {
    return RadioListTile(
      title: Text(value),
      value: value,
      groupValue: _estado,
      onChanged: (String? v) {
        _estado = v!;
        switch (_estado) {
          case "Verde":
            _statusMsg.text = "No hay aviso de personas con covid";
            break;
          case "Amarillo":
            _statusMsg.text =
                "Hay al menos una persona con caso sospechoso de covid, favor de tomar aislamiento preventivo";
            break;
          case "Rojo":
            _statusMsg.text =
                "Hay una persona con caso positivo de covid, favor de tomar aislamiento preventivo";
            break;
        }

        if (Auth.user!.entityType != "Empresa") {
          if (_estado == "Verde") {
            _statusMsg.text +=
                ", las clases continuaran de forma [online/presencial].";
          } else {
            _statusMsg.text += ", las clases continuaran de forma online.";
          }
        }
        setState(() {});
      },
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(title, style: TextStyle(color: Colors.black54, fontSize: 20)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _key,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _title("Burbuja numero ${widget.bubble.numero}"),
            _title("Estado Actual"),
            _estadoRadio("Verde"),
            _estadoRadio("Amarillo"),
            _estadoRadio("Rojo"),
            Visibility(
              visible: widget.bubble.firstDivider.isNotEmpty,
              child: Column(
                children: [
                  _title("Curso ${widget.bubble.firstDivider}"),
                  _title("Division ${widget.bubble.secondDivider}"),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              padding: EdgeInsets.only(bottom: 10),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: _statusMsg,
                maxLines: 5,
                validator: (value) {
                  if (value!.isNotEmpty) {
                    return null;
                  } else {
                    return "El campo no puede estar vacio";
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.email_outlined),
                  labelText: "Mensaje segun el estado",
                ),
              ),
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.green),
              ),
              child: Text("Editar Burbuja"),
              onPressed: () {
                final b = widget.bubble.copyWith(
                  status: _estado,
                  statusMsg: _statusMsg.text,
                  lastUpdate: DateTime.now().millisecondsSinceEpoch.toString(),
                );
                FirebaseController.updateBubble(b, context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
