import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/edit/edit_bubble_form.dart';
import 'package:flutter/material.dart';

class EditBubblePage extends StatelessWidget {
  EditBubblePage(this.bubble);
  final Bubble bubble;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crear burbuja'),
      ),
      body: EditBubbleForm(bubble),
    );
  }
}
