import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/create/new_bubble_form.dart';
import 'package:flutter/material.dart';

class NewBubblePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crear burbuja'),
      ),
      body: NewBubbleForm(),
    );
  }
}
