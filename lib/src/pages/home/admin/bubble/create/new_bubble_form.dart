import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';

class NewBubbleForm extends StatefulWidget {
  @override
  _NewBubbleFormState createState() => _NewBubbleFormState();
}

class _NewBubbleFormState extends State<NewBubbleForm> {
  final _key = GlobalKey<FormState>();
  final _numero = TextEditingController();
  final _statusMsg = TextEditingController();

  String _estado = "Verde";
  String _firstDivider = "1ro";
  String _secondDivider = "1ra";
  bool showFirstDivs = true;
  bool showSecondDivs = true;

  @override
  void initState() {
    super.initState();
    getDividers();

    const String verde = "No hay aviso de personas con covid";
    const String amarillo =
        "Hay al menos una persona con caso sospechoso de covid, favor de tomar aislamiento preventivo";
    const String rojo =
        "Hay una persona con caso positivo de covid, favor de tomar aislamiento preventivo";

    switch (_estado) {
      case "Verde":
        _statusMsg.text = verde;
        break;
      case "Amarillo":
        _statusMsg.text = amarillo;

        break;
      case "Rojo":
        _statusMsg.text = rojo;

        break;
    }

    if (Auth.user!.entityType != "Empresa") {
      _statusMsg.text +=
          ", las clases continuaran de forma [online/presencial].";
    }
  }

  List<String> _secondDividers = [
    "1ra",
    "2da",
    "3ra",
    "4ta",
    "5ta",
    "6ta",
    "7ma",
    "8va"
  ];

  List<String> _firstDividers = [
    "1ro",
    "2do",
    "3ro",
    "4to",
    "5to",
    "6to",
  ];

  void getDividers() {
    setState(() {
      switch (Auth.user!.entityType) {
        case "Primaria":
          _firstDividers.add("7mo");
          _secondDividers = ["A", "B", "C", "D", "E", "F"];
          break;
        case "Secundaria":
          break;
        case "Empresa":
          showFirstDivs = false;
          showSecondDivs = false;
          _firstDividers = [];
          _secondDividers = [];
          break;
      }
    });
  }

  Widget _estadoRadio(String value) {
    return RadioListTile(
      title: Text(value),
      value: value,
      groupValue: _estado,
      onChanged: (String? v) {
        setState(() {
          _estado = v!;
        });
      },
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(title, style: TextStyle(color: Colors.black54, fontSize: 20)),
    );
  }

  Widget _firstDividerDropDownButton() {
    return Visibility(
      visible: showFirstDivs,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 32),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(Icons.class_),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text('Curso:'),
                ),
                DropdownButton<String>(
                  items: _firstDividers
                      .map(
                        (String showedOption) => DropdownMenuItem<String>(
                          value: showedOption,
                          child: Text(showedOption),
                        ),
                      )
                      .toList(),
                  onChanged: (newSelectedOption) {
                    setState(() {
                      _secondDivider = "";
                      _firstDivider = newSelectedOption!;
                    });
                  },
                  value: _firstDivider,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _secondDividerDropDownButton() {
    return Visibility(
      visible: showSecondDivs,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 32),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(Icons.class_),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text('Division:'),
                ),
                DropdownButton<String>(
                  items: _secondDividers
                      .map(
                        (String showedOption) => DropdownMenuItem<String>(
                          value: showedOption,
                          child: Text(showedOption),
                        ),
                      )
                      .toList(),
                  onChanged: (newSelectedOption) {
                    setState(() {
                      _secondDivider = newSelectedOption!;
                    });
                  },
                  value: _secondDivider,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _key,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              padding: EdgeInsets.only(bottom: 10),
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: _numero,
                validator: (value) {
                  if (value!.isNotEmpty) {
                    if (int.parse(value) > 0) {
                      return null;
                    } else {
                      return "El numero debe ser mayor a 0";
                    }
                  } else {
                    return "El campo no puede estar vacio";
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.format_list_numbered),
                  labelText: "Numero de burbuja",
                ),
              ),
            ),
            _title("Estado Actual"),
            _estadoRadio("Verde"),
            _estadoRadio("Amarillo"),
            _estadoRadio("Rojo"),
            _title("Turno"),
            _firstDividerDropDownButton(),
            _secondDividerDropDownButton(),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.green),
              ),
              child: Text("Crear Burbuja"),
              onPressed: () {
                final bubble = Bubble(
                  adminID: Auth.user!.docID,
                  firstDivider: _firstDivider,
                  secondDivider: _secondDivider,
                  status: _estado,
                  statusMsg: _statusMsg.text,
                  numero: _numero.text,
                  lastUpdate: DateTime.now().millisecondsSinceEpoch.toString(),
                );
                FirebaseController.createBubble(bubble, context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
