import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';

class NewUserForm extends StatefulWidget {
  NewUserForm({required this.bubble});
  final Bubble bubble;
  @override
  _NewUserFormState createState() => _NewUserFormState();
}

class _NewUserFormState extends State<NewUserForm> {
  final _key = GlobalKey<FormState>();
  final _email = TextEditingController();
  final _identifier = TextEditingController();

  @override
  void initState() {
    super.initState();
    _email.text = "@${Auth.user!.entityName}.com".toLowerCase();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _key,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              padding: EdgeInsets.only(bottom: 10),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: _email,
                validator: (value) {
                  if (value!.isNotEmpty) {
                    if ((value.contains("@") && (value.contains(".com")))) {
                      return null;
                    } else {
                      return "Debe ingresar un email valido";
                    }
                  } else {
                    return "El campo no puede estar vacio";
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.email_outlined),
                  labelText: "Email",
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: TextFormField(
                keyboardType: TextInputType.text,
                controller: _identifier,
                validator: (value) {
                  if (value!.isNotEmpty) {
                    return null;
                  } else {
                    return "El campo no puede estar vacio";
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.person),
                  labelText: "Identificador",
                  hintText: "Familiares de .., Apellido, Nombre",
                ),
              ),
            ),
            Text("La contraseña será: '12345678', el usuario podra cambiarla."),
            ElevatedButton(
              child: Text("Crear Usuario"),
              onPressed: () => FirebaseController.createUser(
                _email.text,
                _identifier.text,
                widget.bubble.id,
                context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
