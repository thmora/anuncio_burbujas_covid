import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/bubble/create/new_user_form.dart';
import 'package:flutter/material.dart';

class NewUserPage extends StatelessWidget {
  NewUserPage({required this.bubble});
  final Bubble bubble;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crear usuario'),
      ),
      body: NewUserForm(bubble: bubble),
    );
  }
}
