import 'package:anuncio_burbujas_covid/src/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/custom_drawer.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:anuncio_burbujas_covid/src/shared/bubble_extension.dart';
import 'package:anuncio_burbujas_covid/src/services/nav_service.dart';

class AdminHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(title: Text('Inicio - Admin')),
      body: AdminHomePageBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Crear una nueva burbuja",
        onPressed: () => Nav.route(context, "/newBubblePage"),
      ),
    );
  }
}

class AdminHomePageBody extends StatefulWidget {
  @override
  _AdminHomePageBodyState createState() => _AdminHomePageBodyState();
}

class _AdminHomePageBodyState extends State<AdminHomePageBody> {
  List<Bubble> bubbles = [];

  @override
  void initState() {
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    final bs = await FirebaseController.bubbles;
    setState(() {
      bubbles = bs;
    });
  }

  Widget _buildBubbles() {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(
        parent: const BouncingScrollPhysics(),
      ),
      itemCount: bubbles.length,
      itemBuilder: (_, idx) => bubbles[idx].show,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Bubble>>(
      future: FirebaseController.bubbles,
      builder: (BuildContext context, AsyncSnapshot<List<Bubble>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data == null) {
          return Center(child: Text("No hay burbujas"));
        } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
          return RefreshIndicator(
            child: _buildBubbles(),
            onRefresh: () => refresh(),
          );
        } else {
          return LoadingWidget("Buscando Burbujas");
        }
      },
    );
  }
}
