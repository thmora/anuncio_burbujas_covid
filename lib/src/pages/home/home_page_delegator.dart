import 'package:anuncio_burbujas_covid/src/pages/home/user/user_home_page.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/shared/perfiles.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/admin/admin_home_page.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/super_admin/superadmin_home.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';

class HomePageDelegator extends StatelessWidget {
  Widget _delegatedPage() {
    if (Auth.user!.perfil == Perfiles.superadmin.str) {
      return SuperAdminHomePage();
    }
    if (Auth.user!.perfil == Perfiles.admin.str) {
      return AdminHomePage();
    }
    if (Auth.user!.perfil == Perfiles.user.str) {
      return UserHomePage();
    }
    return SizedBox.shrink();
  }

  @override
  Widget build(BuildContext context) => _delegatedPage();
}
