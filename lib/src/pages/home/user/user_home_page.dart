import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/custom_drawer.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:anuncio_burbujas_covid/src/models/bubble.dart';
import 'package:anuncio_burbujas_covid/src/shared/loading_widget.dart';
import 'package:anuncio_burbujas_covid/src/shared/bubble_extension.dart';

class UserHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(title: Text('Inicio')),
      body: FutureBuilder<Bubble>(
        future: FirebaseController.myBubble,
        builder: (_, AsyncSnapshot<Bubble> s) {
          if (s.connectionState == ConnectionState.done) return s.data!.show;
          return LoadingWidget("Buscando burbuja..");
        },
      ),
    );
  }
}
