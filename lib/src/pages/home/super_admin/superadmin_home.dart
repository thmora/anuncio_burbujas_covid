import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:anuncio_burbujas_covid/src/pages/home/custom_drawer.dart';
import 'package:anuncio_burbujas_covid/src/services/nav_service.dart';
import 'package:anuncio_burbujas_covid/src/shared/loading_widget.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';
import 'package:anuncio_burbujas_covid/src/models/user.dart';
import 'package:anuncio_burbujas_covid/src/shared/user_extension.dart';

class SuperAdminHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(title: Text('Administradores habilitados')),
      body: FutureBuilder<QuerySnapshot<User>>(
        future: FirebaseController.admins,
        builder: (_, AsyncSnapshot<QuerySnapshot<User>> s) {
          if (s.connectionState == ConnectionState.done) {
            if (s.data!.docs.isNotEmpty) {
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: s.data!.docs.length,
                itemBuilder: (_, idx) => s.data!.docs[idx].data().show,
              );
            } else {
              return Center(child: Text("No hay admins"));
            }
          }
          return LoadingWidget("Recuperando datos");
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Crear un nuevo administrador",
        onPressed: () => Nav.route(context, "/newAdminPage"),
      ),
    );
  }
}
