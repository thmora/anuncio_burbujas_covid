import 'package:anuncio_burbujas_covid/src/pages/home/super_admin/new_admin_form.dart';
import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:flutter/material.dart';

class NewAdminPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crear un nuevo Admin para ${Auth.user!.entityName}'),
      ),
      body: NewAdminForm(),
    );
  }
}
