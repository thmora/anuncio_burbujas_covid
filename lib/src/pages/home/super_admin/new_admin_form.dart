import 'package:anuncio_burbujas_covid/src/services/auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/services/firebase/firebase_controller.dart';

class NewAdminForm extends StatefulWidget {
  @override
  _NewAdminFormState createState() => _NewAdminFormState();
}

class _NewAdminFormState extends State<NewAdminForm> {
  final _key = GlobalKey<FormState>();
  final _email = TextEditingController();

  @override
  void initState() { 
    super.initState();
    _email.text = "@${Auth.user!.entityName}.com".toLowerCase();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _key,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              padding: EdgeInsets.only(bottom: 10),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: _email,
                validator: (value) {
                  if (value!.isNotEmpty) {
                    if ((value.contains("@") && (value.contains(".com")))) {
                      return null;
                    } else {
                      return "Debe ingresar un email valido";
                    }
                  } else {
                    return "El campo no puede estar vacio";
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.email_outlined),
                  labelText: "Email",
                ),
              ),
            ),
            Text("La contraseña será: '12345678', el administrador podra cambiarla."),
            ElevatedButton(
              child: Text("Crear Admin"),
              onPressed: () =>
                  FirebaseController.createAdmin(_email.text, context),
            ),
          ],
        ),
      ),
    );
  }
}
