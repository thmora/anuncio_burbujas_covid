import 'package:flutter/material.dart';
import 'package:anuncio_burbujas_covid/src/services/shared_preferences/shared_prefs.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:anuncio_burbujas_covid/src/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = SharedPrefs();
  await prefs.initPrefs();
  await Firebase.initializeApp();
  runApp(MyApp());
}
